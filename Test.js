const puppeteer = require('puppeteer-core');
const expect = require('expect-puppeteer');
const axios = require('axios');

const locators = require('./locators');
const { getNum } = require('./utils');

class Test {
    constructor() {
        this.page = null;
        this.context = null;

        this.startNumOfMessages = null;
        this.lastNumOfMessages = null;
    }
    async init() {
        console.info('Init');

        const options = {
            method: 'get',
            url: 'http://127.0.0.1:9222/json/version',
        };

        const response = await axios(options);
        const { webSocketDebuggerUrl } = response.data;
        const browser = await puppeteer.connect({ browserWSEndpoint: webSocketDebuggerUrl, defaultViewport: null, args: ['--start-maximized'] });
        this.context = await browser.createIncognitoBrowserContext();
        this.page = await this.context.newPage();
    }

    async login(userId, password) {
        console.info('Logging in');

        await this.page.goto(locators.url);
        await this.page.setViewport({
            width: 1900,
            height: 1280,
            deviceScaleFactor: 1,
        });

        await expect(this.page).toMatchElement(locators.userId_input);
        await expect(this.page).toFill(locators.userId_input, userId);
        await expect(this.page).toClick(locators.userId_next_btn);
        await this.page.waitForNavigation();

        await expect(this.page).toMatchElement(locators.password_input);
        await expect(this.page).toFill(locators.password_input, password);
        await expect(this.page).toClick(locators.password_next_btn);
        await this.page.waitForNavigation();

        await expect(this.page).toMatchElement(locators.mail);
        await expect(this.page).toClick(locators.mail);
        await this.page.waitForNavigation();

        const incomingMessagesInfo = await this.page.$eval(locators.incoming , element => element.textContent);
        this.startNumOfMessages = getNum(incomingMessagesInfo);
    }

    async sendMessage(opt) {
        console.info('Send message');

        const { email, theme, message } = opt;
        await expect(this.page).toMatchElement(locators.create);
        await expect(this.page).toClick(locators.create);
        await expect(this.page).toFill(locators.toWhom, email);
        await expect(this.page).toFill(locators.themeInput, theme);
        await expect(this.page).toFill(locators.messageInput, message);
        await expect(this.page).toClick(locators.send);
    }

    async checkMessage() {
        console.info('Chesk message');

        const incomingMessagesInfo = await this.page.$eval(locators.incoming , element => element.textContent);
        this.lastNumOfMessages = getNum(incomingMessagesInfo);
        console.log('startNumOfMessages', this.startNumOfMessages);
        console.log('lastNumOfMessages', this.lastNumOfMessages);
        const numOfNewMessages = this.lastNumOfMessages - this.startNumOfMessages;
        console.log('numOfNewMessages', numOfNewMessages);
        if(numOfNewMessages === 10) {
            return true;
        }
        
        return new Error(`Received messages: ${numOfNewMessages}.\nExpected messages: 10`);
    }

    async saveMessageText() {
        console.info('Save message text');
        
        const allMessages = {};
        let childElementNumber;
        if(this.startNumOfMessages < 10) {
            childElementNumber = 2;
        } else {
            childElementNumber = 3;
        }

        for(let i = childElementNumber; i < 13; i++) {
            await expect(this.page).toClick(locators.listItem(i));
            const messageTheme = await this.page.$eval(locators.messageTheme , element => element.textContent);
            const messageText = await this.page.$eval(locators.messageText, element => element.textContent);
            allMessages[messageTheme] = messageText;
            await this.page.goBack();
        }
        
        return allMessages;
    }

    async deleteMessages() {
        console.info('Delete messages');

        await expect(this.page).toClick(locators.highlightMessages);
        await expect(this.page).toClick(locators.lastMessage_btn);
        await expect(this.page).toClick(locators.deleteMessage_btn);
    }

    async dispose() {
        console.info('Dispose browser');
        
        await this.context.close();
    }

}

module.exports = Test;