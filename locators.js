exports.url = 'https://login.yahoo.com/?.lang=en-US&src=homepage&.done=https%3A%2F%2Fwww.yahoo.com%2F&pspid=2023538075&activity=ybar-signin';
// exports.url = 'https://mail.yahoo.com/d/folders/1';

exports.userId_input = '#login-username';
exports.password_input = '#login-passwd';

exports.userId_next_btn = '#login-signin';
exports.password_next_btn = '#login-signin';

exports.mail = '#ybarMailLink';
exports.create = '#app > div.I_ZnwrMC.D_F.em_N.o_h.W_6D6F.H_6D6F > div > div.a_3rehn.W_3o4BO.s_3o4BO.cZ10Gnkk_ZM1sUU.D_F.ek_BB.em_0 > nav > div > div:nth-child(1) > a';
exports.toWhom = '#message-to-field';
exports.themeInput= '#mail-app-component > div > div > div.compose-header.en_0 > div:nth-child(3) > div > div > input';
exports.messageInput = '#editor-container > div.rte.em_N.ir_0.iy_A.iz_h.N_6Fd5';
exports.send = '#mail-app-component > div > div > div.em_N.D_F.ek_BB.p_R.o_h > div:nth-child(2) > div > button';

exports.listItem = (num) => `#mail-app-component > div > div > div.D_F.ab_FT.em_N.ek_BB.iz_A.H_6D6F > div > div > div.W_6D6F.H_6D6F.cZ1RN91d_n.o_h.p_R.em_N.D_F > div > div.p_R.Z_0.iy_h.iz_A.W_6D6F.H_6D6F.k_w.em_N.c22hqzz_GN > ul > li:nth-child(${num})`;

exports.messageTheme = '#mail-app-component > div > div.iz_A.I_52qC.em_0.Z_0 > div:nth-child(1) > header > div.D_F.ab_C.em_N.o_h.A_6FsP.P_3Y3Gk > span > span:nth-child(1)'
exports.messageText = '#mail-app-component > div > div.iz_A.I_52qC.em_0.Z_0 > div:nth-child(2) > ul > li:nth-child(2) > div > div';

exports.incoming = '#app > div.I_ZnwrMC.D_F.em_N.o_h.W_6D6F.H_6D6F > div > div.a_3rehn.W_3o4BO.s_3o4BO.cZ10Gnkk_ZM1sUU.D_F.ek_BB.em_0 > nav > div > div.Y_fq7.P_Z1jXKuU.D_B.iz_A.iy_h.it_68UO > div.folder-list.p_R.k_w.W_6D6F.U_3mS2U > ul > li:nth-child(1) > div > a';
exports.highlightMessages = '#mail-app-component > div > div > div.en_0 > div > div:nth-child(1) > div > div > ul > li:nth-child(1) > span > button > span';
exports.lastMessage_btn = '#mail-app-component > div > div > div.D_F.ab_FT.em_N.ek_BB.iz_A.H_6D6F > div > div > div.W_6D6F.H_6D6F.cZ1RN91d_n.o_h.p_R.em_N.D_F > div > div.p_R.Z_0.iy_h.iz_A.W_6D6F.H_6D6F.k_w.em_N.c22hqzz_GN > ul > li:nth-child(3) > a > div > div.D_F.o_h.ab_C.H_6D6F.a_3vhr3.em_qk.ej_0 > div.en_0.i_0.E_689y.Z_f8a.k_w > button';
exports.deleteMessage_btn = '#mail-app-component > div > div > div.en_0 > div > div.D_F.em_N.gl_C > div > div:nth-child(3) > button';
