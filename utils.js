async function delay(ms) {
    return new Promise((resolve) => setTimeout(resolve, ms));
  }

function getNumOfLettersAndDigits(str) {
    let letters = 0;
    let digits = 0;

    for(let i = 0; i < str.length; i++) {
        if(Number(str[i])) {
            digits++;
        } else {
            letters++;
        }
    }

    return { letters,  digits };
}

function getNum(str) {
    let number = '';
    for(let i = 0; i < str.length; i++) {
        if(Number(str[i]) || str[i] === '0') {
            number += str[i];
        }
    }
    
    return Number(number);
}

function messageGeneration(obj) {
    let textMessage;

    for(const key in obj) {
        const { letters, digits } = getNumOfLettersAndDigits(obj[key]);

        textMessage += `Received mail on theme ${key} \nwith message: ${obj[key]}. \nIt contains ${letters} letters and ${digits} numbers. \n`;
    }

    return textMessage;
}

module.exports = {
    delay,
    getNum,
    messageGeneration,
}

