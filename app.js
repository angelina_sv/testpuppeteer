require('dotenv').config();
const crypto = require("crypto");

const Test = require('./Test');
const {
    delay,
    messageGeneration,
} = require('./utils');

const userId = process.env.USER_ID;
const password = process.env.PASSWORD;
const randomString = () => crypto.randomBytes(10).toString('hex');

const test = new Test();

// async function test1(userId, password) {
//     const options = {
//         email: userId,
//         theme: randomString(),
//         message: randomString(),
//     }

//     await test.init();
//     await test.login(userId, password);
//     await test.sendMessage(options);
// }

// async function test2() {
//     const messages = await test.saveMessageText();
//     const messageText = messageGeneration(messages);
//     const options = {
//         email: userId,
//         theme: randomString(),
//         message: messageText,
//     }
//     await test.sendMessage(options);
// }

// async function runTests() {
//     const promises = [];
//     for(let i = 0; i < 1; i++) {
//         await test1(userId, password).catch((err) => console.log(err));
//     }
 
//     // await delay(180000);
//     await test2();
// }

async function runTests(userId, password) {
    const options = {
        email: userId,
        theme: randomString(),
        message: randomString(),
    }

    await test.init();
    await test.login(userId, password);
    for(let i = 0; i < 10; i++) {
        await test.sendMessage(options);
    }

    console.info('Waiting for messages. It takes about 4 minutes');
    await delay(240000);
    
    await test.checkMessage();

    const messages = await test.saveMessageText();
    const messageText = messageGeneration(messages);
    const newOptions = {
        email: userId,
        theme: randomString(),
        message: messageText,
    }
    await test.sendMessage(newOptions);
    await test.deleteMessages();
}

runTests(userId, password).catch((err) => console.info(err));